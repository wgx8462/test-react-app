import './App.css';
import Test1 from './pages/Test1';
import Test2 from './pages/Test2';
import Test3 from './pages/Test3';
import Test4 from './pages/Test4';
import {Routes, Route} from 'react-router-dom';
import DefaultLayout from "./layousts/DefaultLayout";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<DefaultLayout><Test1 /></DefaultLayout>} />
        <Route path="/2" element={<DefaultLayout><Test2 /></DefaultLayout>} />
          <Route path="/3" element={<DefaultLayout><Test3 /></DefaultLayout>} />
          <Route path="/4" element={<DefaultLayout><Test4 /></DefaultLayout>} />
      </Routes>
    </div>
  );
}

export default App;
