import React from 'react';
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <div>
                헤더

                    <Link to="/">테스트1</Link>
                    |
                    <Link to="/2">테스트2</Link>
                    |
                    <Link to="/3">테스트3</Link>
                    |
                    <Link to="/4">테스트4</Link>

            </div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default DefaultLayout;