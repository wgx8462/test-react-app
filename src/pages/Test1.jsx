import React, {useState} from 'react';

const Test1 = () => {
    const [oneName, setOneName] = useState('')
    const [twoName, setTwoName] = useState('')
    const [score, setScore] = useState(0)


    const handleOneNameChange = e => {
        setOneName(e.target.value)
    }
    const handleTwoNameChange = e => {
        setTwoName(e.target.value)
    }

    const calculateScore = () => {
        setScore(Math.floor(Math.random() * 101))
    }

    return (
        <div>
            <h3>이름의 궁합도</h3>
            <div>
                <input type="text" name="name1" onChange={handleOneNameChange}/>
                <input type="text" name="name2" onChange={handleTwoNameChange}/>
            </div>
            <button onClick={calculateScore}>궁합도 보기</button>
            <div>
                <p>{oneName}와 {twoName}의 궁합도는 {score}%입니다.</p>
            </div>
        </div>
    )
}

export default Test1;