import React, {useState} from 'react';

const Test2 = () => {
    const [userTop, setUserTop] = useState('')
    const [userBottom, setUserBottom] = useState('')
    const [pickTop, setPickTop] = useState('')
    const [pickBottom, setPickBottom] = useState('')

    const handleTopChange = (e) => {
        setUserTop(e.target.value)
    }

    const handleBottomChange = (e) => {
        setUserBottom(e.target.value)
    }

    const pickTopBottom = () => {
        const topArr = userTop.split(',')
        const bottomArr = userBottom.split(',')

        setPickTop(topArr[Math.floor(Math.random() * topArr.length)])
        setPickBottom(bottomArr[Math.floor(Math.random() * bottomArr.length)])
    }

    return (
        <div>
            <h3>
                내일 입을 옷 추천
            </h3>
            <div>
                <input placeholder={"상의"} type="text" value={userTop} onChange={handleTopChange}/>
            </div>
            <div>
                <input placeholder={"하의"} type="text" value={userBottom} onChange={handleBottomChange}/>
            </div>
            <button onClick={pickTopBottom}>옷 추천</button>

            <p>당신은 내일 {pickTop} {pickBottom}을(를) 입어라</p>
        </div>
    )
}
export default Test2;