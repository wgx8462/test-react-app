import React, {useState} from 'react';

const Test3 = () => {
    const INIT_PRICE = 50000
    const BOX_PRICE = 5000

    const [price, setPrice] = useState(INIT_PRICE)
    const [boxItem, setBoxItem] = useState([])

    const items = [
        {id: 1, name: '뽀쏭 치약', percent: 0.165},
        {id: 2, name: '내끄야 바지', percent: 0.165},
        {id: 3, name: '우웩 향수', percent: 0.165},
        {id: 4, name: '메롱 사탕', percent: 0.165},
        {id: 5, name: '잠이온다 귀마개', percent: 0.165},
        {id: 6, name: '아프지않다 후시딘', percent: 0.165},
        {id: 7, name: '10억', percent: 0.01}
    ]

    const getRandomItem = () => {
        const random = Math.random()
        let sum = 0
        for (const item of items) {
            sum += item.percent
            if (random < sum) return item
        }
    }

    const boxOpen = () => {
        if (price < BOX_PRICE) return false

        setPrice(price - BOX_PRICE)
        const oneItem = getRandomItem()
        setBoxItem([...boxItem, oneItem])

    }

    const openReset = () => {
        setPrice(INIT_PRICE)
        setBoxItem([])
    }

    return (
        <div>
            <h3>랜덤 상자 열기</h3>
            <div>보유 금액 {price}원</div>
            <button onClick={boxOpen}>상자 열기</button>
            <div>
                {boxItem.map((itme, index) => (
                    <div key={itme.id}>
                        {index + 1 + '.'}{itme.name} 축하합니다.
                    </div>
                ))}
            </div>
            {price === 0 ? <button onClick={openReset}>다시 하기</button> : ''}
        </div>
    )
}
export default Test3;