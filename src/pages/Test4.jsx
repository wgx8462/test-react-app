import React, {useState} from 'react';

const Test4 = () => {
    const [price, setPrice] = useState(0)
    const [oneName, setOneName] = useState('')
    const [twoName, setTwoName] = useState('')
    const [threeName, setThreeName] = useState('')
    const [addPrice, setAddPrice] = useState([])

    const handlePrice = e => {
        setPrice(e.target.value)
        setAddPrice([])
    }

    const handleOneName = e => {
        setOneName(e.target.value)
    }

    const handleTwoName = e => {
        setTwoName(e.target.value)
    }

    const handleThreeName = e => {
        setThreeName(e.target.value)
    }

    const randomChange = () => {
        const max1 = Math.floor(Math.random() * 101)
        const max2 = Math.floor(Math.random() * (100 - max1))

        let add1 = price * (max1 * 0.01)
        let price1 = price - add1
        let add2 = price1 * (max2 * 0.01)
        let add3 = price1 - add2


        setAddPrice([...addPrice, add1, add2, add3])
    }

    return (
        <div>
            <div>
                복불복 금액 : <input type="number" value={price} onChange={handlePrice}/>
            </div>
            <div>
                1.<input type="text" value={oneName} onChange={handleOneName}/>
                2.<input type="text" value={twoName} onChange={handleTwoName}/>
                3.<input type="text" value={threeName} onChange={handleThreeName}/>
            </div>
            <button onClick={randomChange}>나만 아니면돼!!!</button>
            <p>{oneName} : {addPrice[0]}원 <br/>{twoName} : {addPrice[1]}원 <br/>{threeName} : {addPrice[2]}원</p>
        </div>
    )
}
export default Test4;